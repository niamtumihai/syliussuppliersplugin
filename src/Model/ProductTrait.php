<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Model;

use Doctrine\ORM\Mapping as ORM;

trait ProductTrait
{
    /**
     * @var SupplierInterface
     *
     * @ORM\ManyToOne(targetEntity="\Blackowl\SyliusSupplierPlugin\Model\SupplierInterface", cascade={"persist"}, fetch="EAGER", inversedBy="products")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $supplier;

    /**
     * @return SupplierInterface|null
     */
    public function getSupplier(): ?SupplierInterface
    {
        return $this->supplier;
    }

    /**
     * @param SupplierInterface|null $supplier
     */
    public function setSupplier(?SupplierInterface $supplier): void
    {
        $this->supplier = $supplier;
    }
}
