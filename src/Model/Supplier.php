<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Model;

class Supplier implements SupplierInterface {

    use ProductsAwareTrait {
        ProductsAwareTrait::__construct as private __productsAwareTraitConstruct;
    }

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $code;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $address;

    /**
     * @var string|null
     */
    protected $email;
    
        /**
     * @var string|null
     */
    protected $vat;

    /**
     * @var string|null
     */
    protected $tel;

    public function __construct() {
        $this->__productsAwareTraitConstruct();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString(): string {
        return (string) $this->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): ?string {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(?string $code): void {
        $this->code = $code;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    function getAddress(): ?string {
        return $this->address;
    }

    function getTel(): ?string {
        return $this->tel;
    }

    function setAddress(?string $address): void {
        $this->address = $address;
    }

    function setTel(?string $tel): void {
        $this->tel = $tel;
    }

    function getEmail(): ?string {
        return $this->email;
    }

    function setEmail(?string $email): void {
        $this->email = $email;
    }
    
    function getVat(): ?string {
        return $this->vat;
    }

    function setVat(?string $vat): void {
        $this->vat = $vat;
    }

    public function addProduct(ProductInterface $product): void {
        if (!$this->hasProduct($product)) {
            $product->setSupplier($this);
            $this->products->add($product);
        }
    }

    public function removeProduct(ProductInterface $product): void {
        if ($this->hasProduct($product)) {
            $product->setSupplier(null);
            $this->products->removeElement($product);
        }
    }
   
}
