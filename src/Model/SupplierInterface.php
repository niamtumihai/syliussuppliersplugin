<?php
declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Model;

use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface SupplierInterface extends ResourceInterface, CodeAwareInterface, ProductsAwareInterface{
    /**
     * Returns the name of the supplier
     *
     * @return string
     */
    public function __toString(): string;
    /**
     * @return int
     */
    public function getId(): ?int;

    /**
     * @return string|null
     */
    public function getName(): ?string;
    /**
     * @param string|null $name
     */
    public function setName(?string $name): void;
    /**
     * @return string|null
     */
    public function getAddress(): ?string;
    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void;
    /**
     * @return string|null
     */
    public function getEmail(): ?string;
    /**
     * @param string|null $email
     */
    public function setEmail(?string $address): void;
    /**
     * @return int|null
     */
    public function getTel(): ?string;
    /**
     * @param int|null $email
     */
    public function setTel(?string $tel): void;
    
    public function getVat(): ?string;

    public function setVat(?string $vat): void;
}
