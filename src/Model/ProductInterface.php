<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Model;

use Sylius\Component\Core\Model\ProductInterface as BaseProductInterface;

interface ProductInterface extends BaseProductInterface, SupplierAwareInterface
{
}
