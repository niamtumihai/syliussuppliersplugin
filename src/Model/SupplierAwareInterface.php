<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Model;

interface SupplierAwareInterface
{
    /**
     * @return SupplierInterface|null
     */
    public function getSupplier(): ?SupplierInterface;

    /**
     * @param SupplierInterface|null $supplier
     */
    public function setSupplier(?SupplierInterface $supplier): void;
}
