<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Event;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class SupplierMenuBuilderEvent extends MenuBuilderEvent
{
    /** @var SupplierInterface */
    private $supplier;

    public function __construct(FactoryInterface $factory, ItemInterface $menu, SupplierInterface $supplier)
    {
        parent::__construct($factory, $menu);

        $this->supplier = $supplier;
    }

    public function getSupplier(): SupplierInterface
    {
        return $this->supplier;
    }
}
