<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\EventListener;

use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Resource\Exception\UnexpectedTypeException;

final class SupplierDeletionListener
{
    /**
     * Prevent supplier deletion if it used in product
     *
     * @param ResourceControllerEvent $event
     */
    public function onSupplierPreDelete(ResourceControllerEvent $event): void
    {
        $supplier = $event->getSubject();

        if (!$supplier instanceof SupplierInterface) {
            throw new UnexpectedTypeException(
                $supplier,
                SupplierInterface::class
            );
        }

        if ($supplier->hasProducts()) {
            $event->stop('blackowl_sylius_supplier.supplier.delete_error');
        }
    }
}
