<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Doctrine\ORM;

use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class SupplierRepository extends EntityRepository implements SupplierRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findByPhrase(string $phrase): array
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.name LIKE :phrase OR o.code LIKE :phrase')
            ->setParameter('phrase', '%' . $phrase . '%')
            ->orderBy('o.name', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllSuppliers():array
    {
        return $this->createQueryBuilder('o')
            ->orderBy('o.name','ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByC($code):array
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->andWhere('o.code = :phrase')
            ->setParameter('phrase', $code[0] )
            ->orderBy('o.name','ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
