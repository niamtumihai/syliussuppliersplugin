<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Doctrine\ORM;

use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface SupplierRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $phrase
     *
     * @return array|SupplierInterface[]
     */
    public function findByPhrase(string $phrase): array;
}
