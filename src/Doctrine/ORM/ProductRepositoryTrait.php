<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Doctrine\ORM;

use Doctrine\ORM\QueryBuilder;
use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;

trait ProductRepositoryTrait
{
    /**
     * @param string $alias
     * @param string|null $indexBy The index for the from.
     *
     * @return QueryBuilder
     */
    abstract public function createQueryBuilder($alias, $indexBy = null);

    /**
     * {@inheritdoc}
     */
    public function createPaginatorForSupplier(SupplierInterface $supplier): iterable
    {
        return $this->createQueryBuilder('o')
            ->where('o.supplier = :supplier')
            ->setParameter('supplier', $supplier)
            ->getQuery()
            ->getResult()
            ;
    }
}
