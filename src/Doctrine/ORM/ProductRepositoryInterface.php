<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Doctrine\ORM;

use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface as BaseProductRepositoryInterface;

interface ProductRepositoryInterface extends BaseProductRepositoryInterface
{
    /**
     * @param SupplierInterface $supplier
     *
     * @return iterable
     */
    public function createPaginatorForSupplier(SupplierInterface $supplier): iterable;
}
