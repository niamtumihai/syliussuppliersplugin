<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Menu;

use Sylius\Bundle\AdminBundle\Event\ProductMenuBuilderEvent;

final class AdminProductFormMenuListener
{
    /**
     * @param ProductMenuBuilderEvent $event
     */
    public function addItems(ProductMenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $menu
            ->addChild('supplier')
            ->setAttribute('template', '@BlackowlSyliusSupplierPlugin/Admin/Product/_supplier.html.twig')
            ->setLabel('blackowl_sylius_supplier.ui.supplier')
        ;
    }
}
