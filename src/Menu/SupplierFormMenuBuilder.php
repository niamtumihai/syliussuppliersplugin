<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Blackowl\SyliusSupplierPlugin\Event\SupplierMenuBuilderEvent;
use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\LegacyEventDispatcherProxy;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface as ContractsEventDispatcherInterface;

final class SupplierFormMenuBuilder
{
    public const EVENT_NAME = 'blackowl_sylius_supplier.menu.admin.supplier.form';

    /** @var FactoryInterface */
    private $factory;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    public function __construct(FactoryInterface $factory, EventDispatcherInterface $eventDispatcher)
    {
        $this->factory = $factory;

        if (class_exists('Symfony\Component\EventDispatcher\LegacyEventDispatcherProxy')) {
            /**
             * It could return null only if we pass null, but we pass not null in any case
             *
             * @var ContractsEventDispatcherInterface
             */
            $eventDispatcher = LegacyEventDispatcherProxy::decorate($eventDispatcher);
        }

        $this->eventDispatcher = $eventDispatcher;
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!array_key_exists('supplier', $options) || !$options['supplier'] instanceof SupplierInterface) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', '@BlackowlSyliusSupplierPlugin/Admin/Supplier/Tab/_details.html.twig')
            ->setLabel('sylius.ui.details')
            ->setCurrent(true)
        ;

        if (class_exists('Symfony\Component\EventDispatcher\LegacyEventDispatcherProxy')) {
            $this->eventDispatcher->dispatch(
                new SupplierMenuBuilderEvent($this->factory, $menu, $options['supplier']),
                self::EVENT_NAME
            );
        } else {
            $this->eventDispatcher->dispatch(
                self::EVENT_NAME,
                new SupplierMenuBuilderEvent($this->factory, $menu, $options['supplier'])
            );
        }

        return $menu;
    }
}
