<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Form\Extension\Api;

use Blackowl\SyliusSupplierPlugin\Form\Type\SupplierChoiceType;
use Sylius\Bundle\AdminApiBundle\Form\Type\ProductType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

class ProductTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('supplier', SupplierChoiceType::class, [
            'placeholder' => 'blackowl_sylius_supplier.form.product.select_supplier',
            'label' => 'blackowl_sylius_supplier.form.product.supplier',
            'required' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes():iterable
    {
        return [
            ProductType::class
        ];
    }
}
