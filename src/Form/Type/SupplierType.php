<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Form\Type;

use Sylius\Bundle\ResourceBundle\Form\EventSubscriber\AddCodeFormSubscriber;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class SupplierType extends AbstractResourceType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventSubscriber(new AddCodeFormSubscriber(null, [
                'label' => 'blackowl_sylius_supplier.form.supplier.code',
            ]))
            ->add('name', TextType::class, [
                'label' => 'blackowl_sylius_supplier.form.supplier.name',
            ])
            ->add('vat', TextType::class, [
                'label' => 'blackowl_sylius_supplier.form.supplier.vat',
            ])
            ->add('address', TextType::class, [
                'label' => 'blackowl_sylius_supplier.form.supplier.address',
            ])
            ->add('email', TextType::class, [
                'label' => 'blackowl_sylius_supplier.form.supplier.email',
            ])
            ->add('tel', TextType::class, [
                'label' => 'blackowl_sylius_supplier.form.supplier.tel',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'blackowl_sylius_supplier_supplier';
    }
}
