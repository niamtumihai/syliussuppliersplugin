<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Assigner;

use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;
use Blackowl\SyliusSupplierPlugin\Model\ProductInterface;

interface ProductsAssignerInterface
{
    /**
     * @param SupplierInterface $supplier
     * @param ProductInterface[]|array $products
     */
    public function assign(SupplierInterface $supplier, array $products): void;
}
