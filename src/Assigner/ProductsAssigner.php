<?php

declare(strict_types=1);

namespace Blackowl\SyliusSupplierPlugin\Assigner;

use Blackowl\SyliusSupplierPlugin\Model\SupplierInterface;
use Blackowl\SyliusSupplierPlugin\Model\ProductInterface;

final class ProductsAssigner implements ProductsAssignerInterface
{
    /**
     * {@inheritdoc}
     */
    public function assign(SupplierInterface $supplier, array $products): void
    {
        foreach ($products as $product) {
            if (!$product instanceof ProductInterface) {
                throw new \RuntimeException(sprintf(
                    "Some product was not found to assign to supplier '%s'",
                    $supplier->getCode()
                ));
            }

            $supplier->addProduct($product);
        }
    }
}
